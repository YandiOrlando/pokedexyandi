//
//  ExtensionUIView.swift
//  pokedex
//
//  Created by COTEMIG on 26/05/22.
//

import Foundation
import UIKit

extension UIView{
    
    func roundCorners(cornerRadius:CGFloat, typeCorners:CACornerMask){
        self.layer.cornerRadius = cornerRadius
        self.layer.maskedCorners = typeCorners
        self.clipsToBounds = true
    }
}

extension CACornerMask{
    static public let inferiorDireito:CACornerMask = .layerMaxXMaxYCorner
    static public let inferiorEsquerdo:CACornerMask = .layerMinXMaxYCorner
    static public let superiorDireito:CACornerMask = .layerMaxXMinYCorner
    static public let superiorEsquerdo:CACornerMask = .layerMinXMinYCorner
}

