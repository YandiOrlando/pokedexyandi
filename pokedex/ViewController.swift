//
//  ViewController.swift
//  pokedex
//
//  Created by COTEMIG on 24/02/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var BtnPokedex: UIButton!
    @IBOutlet weak var BtnMovimentos: UIButton!
    @IBOutlet weak var BtnHabilidades: UIButton!
    @IBOutlet weak var BtnTypeCharts: UIButton!
    @IBOutlet weak var BtnLocalizacao: UIButton!
    @IBOutlet weak var BtnItens: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createShadow(btn: BtnPokedex)
        createShadow(btn: BtnMovimentos)
        createShadow(btn: BtnHabilidades)
        createShadow(btn: BtnTypeCharts)
        createShadow(btn: BtnLocalizacao)
        createShadow(btn: BtnItens)
    }
    
    func createShadow(btn: UIButton){
        btn.layer.shadowColor = btn.backgroundColor?.cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 5)
        btn.layer.shadowRadius = 5
        btn.layer.shadowOpacity = 0.7
    }


}

